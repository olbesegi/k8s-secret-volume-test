# k8s-secret-volume-test
Test of mounting secrets in kubernetes

Need a running kubernetes cluster 

    kubectl create secret generic wildfly-mgmt --from-file=mgmt-groups.properties --from-file=mgmt-users.properties
    kubectl apply -f redis.yaml
    kubectl apply -f mypod.yaml
    kubectl exec  redis -- ls /mnt/secrets
    kubectl exec  mypod -- ls /mnt/secrets
    

